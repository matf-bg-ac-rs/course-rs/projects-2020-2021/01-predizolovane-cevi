# Project 01-Predizolovane-cevi

Aplikaciji koja bi sluzila za razlicita izracunavanja vezana za projektovanje cevovoda. Osnovna poglavlja iz kojih bi se sastojao program: 
1) Proračun toplotnih gubitaka 
2) Dimenzionisanje cevi 
3) Proračun dužine polaganja cevovoda 
4) Proračun kompenzacionih jastuka – hladno polaganje 
5) Proračun kompenzacionih jastuka na ograncima 
6) Proračun kompenzacionih jastuka - predgrevanje 
7) Proračun jednovremenih kompenzatora

## Nedeljni izveštaji

[Link do izveštaja](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/01-predizolovane-cevi/-/wikis/Izve%C5%A1taji)

## Developers

- [Dušan Petrović, 14/2017](https://gitlab.com/dpns98)
- [Aleksa Tešić, 121/2017](https://gitlab.com/Imafikus)
- [Nikola Šutić, 121/2018](https://gitlab.com/nikolasutic) - odustao
- [Luka Colić, 31/2018](https://gitlab.com/LCEnzo) - odustao 
- [Filip Božović, 43/2017](https://gitlab.com/Smrznuti)

## O projektu 

Kako se trenutno sva izračunavanja vrše uz pomoć ogromnih excel tabela, motivacija za rađenje ovog projekta je bila pravljenje nekakvog efikasnijeg i intuitivnijeg sistema koji bi vršio ta izračunavanja i koji bi zamenio postojeće excel tabele.

Projekat se sastoji iz dva glavna dela:
- GKI 
- Zaglavlja koja vrše izračunavanja

### Dokumentacija

Dokumentacija i opis svih traženih funkcionalnosti se može naći u folderu `Documentation` koji se nalazi u korenu ovog repozitorijuma. Unutar tog foldera se nalaze i stare excel tabele koje ova aplikacija treba da zameni

### Fajlovi neophodni za rad aplikacije 

Kako bi izračunavanja bila uspešna potrebno je imati fajlove koji opisuju već poznate parametre koji se koriste pri izračunavanju. 
Ovi fajlovi se nalaze u `src/data` folderu 

## Opis GKI

GKI je implementiran korišćenjem QT5 biblioteke za c++ i sastoji se od 10 tabova i posebnog dela koji je deljen između tabova. 

### Home tab

![Home tab](https://i.postimg.cc/DZPKvBZv/homeTab.png)

### Energy Loss tab

![Energy Loss tab](https://i.postimg.cc/SNsp4Lp4/energy-Loss-Tab.png)

### Pressure Loss tab

![Pressure Loss tab](https://i.postimg.cc/jqfM7LQf/pressure-Loss-Tab.png)


### Frictional Length tab

![Frictional Length tab](https://i.postimg.cc/kgTsm890/frictional-Length-Tab.png)


### Foam Pads L tab

![Foam Pads L tab](https://i.postimg.cc/1RDKzbsc/foam-Pads-LTab.png)


### Foam Pads Z tab

![Foam Pads Z tab](https://i.postimg.cc/tCyNnDg0/foam-Pads-ZTab.png)


### Foam Pads U tab

![Foam Pads U tab](https://i.postimg.cc/9QQPJMTJ/foam-Pads-UTab.png)

### Foam Pads T tab

![Foam Pads T tab](https://i.postimg.cc/tg13GMB6/foam-Pads-TTab.png)

### Foam Pads P tab

![Foam Pads P tab](https://i.postimg.cc/T2cWFxTS/foam-Pads-PTTab.png)

### E compensation tab

![E compensation tab](https://i.postimg.cc/wj1y1sD8/foam-Pads-ETab.png)

## Pokretanje projekta 

Kako je projekat bio implementiran u razvojnom okruženju QT Creator, dovoljno je kopirati repozitorijum lokalno, zatim pokrenuti QT Creator i odabrati repozitorijum kao projekat koji želimo da otvorimo. Zatim je potrebno da u  `src` folderu nađemo `main.pro` fajl i učitamo projekat.  

Kompajliranje projekta se takođe izvršava preko QT Creator okruženja.

## Video

![Video](Documentation/video.mp4)
