#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <vector>
#include <QMessageBox>

#include "../headers/Pipe.hpp"
#include "../headers/SinglePipe.hpp"
#include "../headers/TwinPipe.hpp"
#include "../headers/Project.hpp"
#include "../headers/FrictionalLength.hpp"
#include "../headers/Lcompesation.hpp"
#include "../headers/Zcompesation.hpp"
#include "../headers/Ucompesation.hpp"
#include "../headers/Tcompesation.hpp"
#include "../headers/Pcompesation.hpp"
#include "../headers/Ecompesation.hpp"
#include "../headers/PressureLoss.hpp"
#include "../headers/EnergyLoss.hpp"
#include "../headers/ErrorMessage.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    disableTabs();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::disableTabs()
{
    ui->tabEnergyLoss->setDisabled(true);
    ui->tabPressureLoss->setDisabled(true);
    ui->tabFrictionalLength->setDisabled(true);
    ui->tabFoamPadsL->setDisabled(true);
    ui->tabFoamPadsZ->setDisabled(true);
    ui->tabFoamPadsU->setDisabled(true);
    ui->tabFoamPadsT->setDisabled(true);
    ui->tabFoamPadsPT->setDisabled(true);
    ui->tabFoamPadsE->setDisabled(true);
}

void MainWindow::enableTabs()
{
    ui->tabEnergyLoss->setDisabled(false);
    ui->tabPressureLoss->setDisabled(false);
    ui->tabFrictionalLength->setDisabled(false);
    ui->tabFoamPadsL->setDisabled(false);
    ui->tabFoamPadsZ->setDisabled(false);
    ui->tabFoamPadsU->setDisabled(false);
    ui->tabFoamPadsT->setDisabled(false);
    ui->tabFoamPadsPT->setDisabled(false);
    ui->tabFoamPadsE->setDisabled(false);
}

void MainWindow::getSharedData()
{
    CoverDepth = ui->leCoverDepth->text().toDouble();
    DN = ui->cbNominalSize->currentText().remove(0, 3).toInt();
    Series = ui->cbInstallationClass->currentText().remove(0, 7).toInt();
    if(ui->cbPipeType->currentText() == "Single pipe")
        type = 1;
    else
        type = 2;
}


ErrorMessage MainWindow::tab1CheckInput()
{
    std::vector<QLineEdit*> tab1QLineEdits = {
        ui->tab1leFlowTemp,
        ui->tab1leReturnTemp,
        ui->tab1leInstallTemp,
        ui->tab1leSoilDensity,
        ui->tab1leFrictionAngle,
        ui->tab1leWaterPressure
    };

    std::vector<QString> tab1QLabelTexts = {
        ui->tab1lblFlowTemp->text(),
        ui->tab1lblReturnTemp->text(),
        ui->tab1lblInstallTemp->text(),
        ui->tab1lblSoilDensity->text(),
        ui->tab1lblFrictionAngle->text(),
        ui->tab1lblWaterPressure->text()
    };

    return ErrorMessage(tab1QLineEdits, tab1QLabelTexts);
}


ErrorMessage MainWindow::tab2CheckInput()
{
    std::vector<QLineEdit*> tab2QLineEdits = {
        ui->tab2leRouteLength, ui->tab2leCreditInterest, ui->tab2leInflationRate,
        ui->tab2lePriceRiseDueToEnergy, ui->tab2leUsageTime, ui->tab2leProductCosts, ui->tab2leTimeFactor, ui->leCoverDepth
    };

    std::vector<QString> tab2QLabelTexts = {
        ui->tab2lblRouteLength->text(), ui->tab2lblCreditInterest->text(), ui->tab2lblInflationRate->text(),
        ui->tab2lblPriceRiseDuteToEnergy->text(), ui->tab2lblUsageTime->text(), ui->tab2lblProductCost->text(), ui->tab2lblTimeFactor->text(), ui->lblCoverDepth->text()
    };

    return ErrorMessage(tab2QLineEdits, tab2QLabelTexts);
}

ErrorMessage MainWindow::tab3CheckInput()
{
    std::vector<QLineEdit*> tab3QLineEdits = {
        ui->tab3leLength,
        ui->tab3leQ,
        ui->tab3ledwConstraint,
        ui->tab3leRConstraint,
        ui->leCoverDepth
    };

    std::vector<QString> tab3QLabelTexts = {
        ui->tab3lblLength->text(),
        ui->tab3lblQ->text(),
        ui->tab3lbldwConstraint->text(),
        ui->tab3lblRConstraint->text(),
        ui->lblCoverDepth->text()
    };

   return ErrorMessage(tab3QLineEdits, tab3QLabelTexts);
}

ErrorMessage MainWindow::tab4CheckInput()
{
    std::vector<QLineEdit*> tab5QLineEdits = {
        ui->leCoverDepth
    };

    std::vector<QString> tab5QLabelTexts = {
        ui->lblCoverDepth->text(),
    };


    return ErrorMessage(tab5QLineEdits, tab5QLabelTexts);
}

ErrorMessage MainWindow::tab5CheckInput()
{
    std::vector<QLineEdit*> tab5QLineEdits = {
        ui->tab5leDistToL1,
        ui->tab5leDistToL2,
        ui->tab5leAngle,
        ui->leCoverDepth
    };

    std::vector<QString> tab5QLabelTexts = {
        ui->tab5lblDistToL1->text(),
        ui->tab5lblDistToL2->text(),
        ui->tab5lblAngle->text(),
        ui->lblCoverDepth->text(),
    };


    return ErrorMessage(tab5QLineEdits, tab5QLabelTexts);
}

ErrorMessage MainWindow::tab6CheckInput()
{
    std::vector<QLineEdit*> tab6QLineEdits = {
        ui->tab6leDistToL1,
        ui->tab6leDistToL2,
        ui->leCoverDepth
    };

    std::vector<QString> tab6QLabelTexts = {
        ui->tab6lblDistToL1->text(),
        ui->tab6lblDistToL2->text(),
        ui->lblCoverDepth->text()
    };

    return ErrorMessage(tab6QLineEdits, tab6QLabelTexts);
}

ErrorMessage MainWindow::tab7CheckInput()
{
    std::vector<QLineEdit*> tab7QLineEdits = {
        ui->tab7leDistToL1,
        ui->tab7leDistToL2,
        ui->leCoverDepth
    };

    std::vector<QString> tab7QLabelTexts = {
        ui->tab7lblDistToL1->text(),
        ui->tab7lblDistToL2->text(),
        ui->lblCoverDepth->text()
    };

    return ErrorMessage(tab7QLineEdits, tab7QLabelTexts);
}

ErrorMessage MainWindow::tab8CheckInput()
{
    std::vector<QLineEdit*> tab8QLineEdits = {
        ui->tab8leCoverDepthMain,
        ui->tab8leCoverDepthBranch,
        ui->tab8leLMain,
        ui->tab8leLa,
        ui->tab8leLt,
    };

    std::vector<QString> tab8QLabelTexts = {
        ui->tab8lblCoverDepthMain->text(),
        ui->tab8lblCoverDepthBranch->text(),
        ui->tab8lblLMain->text(),
        ui->tab8lblLaBranch->text(),
        ui->tab8lblLtBranch->text(),
    };

    return ErrorMessage(tab8QLineEdits, tab8QLabelTexts);
}

ErrorMessage MainWindow::tab9CheckInput()
{
    std::vector<QLineEdit*> tab9QLineEdits = {
        ui->tab9leCoverDepthMain,
        ui->tab9leCoverDepthBranch,
        ui->tab9leLMain,
        ui->tab9leLa,
        ui->tab9leLt,
    };

    std::vector<QString> tab9QLabelTexts = {
        ui->tab9lblCoverDepthMain->text(),
        ui->tab9lblCoverDepthBranch->text(),
        ui->tab9lblLMain->text(),
        ui->tab9lblLaBranch->text(),
        ui->tab9lblLBranch->text(),
    };

    return ErrorMessage(tab9QLineEdits, tab9QLabelTexts);
}


ErrorMessage MainWindow::tab10CheckInput()
{
    std::vector<QLineEdit*> tab10QLineEdits = {
        ui->tab10leDistToL1,
        ui->leCoverDepth
    };

    std::vector<QString> tab10QLabelTexts = {
        ui->tab10lblDistToL1->text(),
        ui->lblCoverDepth->text()
    };

    return ErrorMessage(tab10QLineEdits, tab10QLabelTexts);
}



void MainWindow::on_tab1btnApply_clicked()
{
    auto error = tab1CheckInput();
    if(error.hasInputError()) {
         MainWindow::disableTabs();
         error.displayEmptyInputMessage();
         return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::enableTabs();

    Project = ui->tab1leProject->text();
    CalculationRefNumber = ui->tab1leCalcReferenceNumber->text().toInt();
    CalculationRefNumberMark = ui->tab1leCalcReferenceNumber2->text();
    CalculationsCarriedOutBy = ui->tab1leCalcCarriedOutBy->text();
    Date = ui->tab1leDate->text();

    FlowTemperature = ui->tab1leFlowTemp->text().toDouble();
    ReturnTemperature = ui->tab1leReturnTemp->text().toDouble();
    InstallTemperature = ui->tab1leInstallTemp->text().toDouble();
    SoilDensity = ui->tab1leSoilDensity->text().toDouble();
    InternalFrictionAngleOfSoil = ui->tab1leFrictionAngle->text().toDouble();
    DesignWaterPressure = ui->tab1leWaterPressure->text().toDouble();

}

void MainWindow::on_tab2btnCalculate_clicked()
{
    auto error = tab2CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    double routeLength = ui->tab2leRouteLength->text().toDouble();

    Wetness wetness;
    const std::string strWetness = ui->tab2cbWetness->currentText().toStdString();
    if (strWetness == "Dry") {
        wetness = dry;
    } else if (strWetness == "Medium") {
        wetness = medium;
    } else {
        wetness = wet;
    }

    double creditInterest = ui->tab2leCreditInterest->text().toDouble();
    double inflationRate = ui->tab2leInflationRate->text().toDouble();
    double priceRiseDueToEnergy = ui->tab2lePriceRiseDueToEnergy->text().toDouble();
    int usageTime = ui->tab2leUsageTime->text().toInt();
    double productCosts = ui->tab2leProductCosts->text().toDouble();
    int timeFactor = ui->tab2leTimeFactor->text().toInt();


    EnergyLoss el (FlowTemperature, ReturnTemperature, InstallTemperature,
                   SoilDensity, InternalFrictionAngleOfSoil, DesignWaterPressure,
                   type, Series, DN, CoverDepth, CalculationRefNumber, routeLength, wetness,
                   creditInterest, inflationRate, priceRiseDueToEnergy, usageTime, productCosts, timeFactor);


    ui->tab2leTotalHeatLoss->setText(QString::number(el.q()));
    ui->tab2leCapitalValue->setText(QString::number(el.K()));

    std::string warningUsageTime = el.WarningUsageTime();
    std::string warningTimeFactor = el.WarningTimeFactor();

    if(warningUsageTime.length() || warningTimeFactor.length()) {
        error.displayWarningMessage(warningUsageTime + warningTimeFactor);
    }
}

void MainWindow::on_tab3btnCalculate_clicked()
{
    auto error = tab3CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    double Q = ui->tab3leQ->text().toDouble();
    double length = ui->tab3leLength->text().toDouble();
    PressureLoss pl (FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity,
                     InternalFrictionAngleOfSoil, DesignWaterPressure, type, Series, DN, CoverDepth, length, Q);

    ui->tab3leR->setText(QString::number(pl.R()));
    ui->tab3ledU->setText(QString::number(pl.du()));
    ui->tab3leV->setText(QString::number(pl.V()));
    ui->tab3leRxL->setText(QString::number(pl.RxL()));

    std::string warningW = pl.WarningW(ui->tab3ledwConstraint->text().toDouble());
    std::string warningR = pl.WarningR(ui->tab3leRConstraint->text().toDouble());

    if(warningW.length() || warningR.length()) {
        error.displayWarningMessage(warningW + warningR);
    }
}

void MainWindow::on_tab4btnCalculate_clicked()
{
    auto error = tab3CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    FrictionalLength f(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                       DesignWaterPressure, type, Series, DN, CoverDepth);

    ui->tab4leFrictionForce->setText(QString::number(f.FrictionForce()));
    ui->tab4leInstallationLength->setText(QString::number(f.FrictionLength()));
}

void MainWindow::on_tab5btnCalculate_clicked()
{
    auto error = tab5CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    double L1 = ui->tab5leDistToL1->text().toDouble();
    double L2 = ui->tab5leDistToL2->text().toDouble();
    double angle = ui->tab5leAngle->text().toDouble();
    Lcompesation l(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                   DesignWaterPressure, type, Series, DN, CoverDepth, L1, L2, angle);

    ui->tab5leFoamPads1L1->setText(QString::number(l.FoamPads1(L1, L2)));
    ui->tab5leFoamPads2L1->setText(QString::number(l.FoamPads2()));
    ui->tab5leFoamPads3L1->setText(QString::number(l.FoamPads3()));

    ui->tab5leFoamPads1L2->setText(QString::number(l.FoamPads1(L2, L1)));
    ui->tab5leFoamPads2L2->setText(QString::number(l.FoamPads2()));
    ui->tab5leFoamPads3L2->setText(QString::number(l.FoamPads3()));

    ui->tab5leDeltaL1->setText(QString::number(l.DeltaExpansion(L1, L2)));
    ui->tab5leDeltaL1_2->setText(QString::number(l.DeltaExpansion(L2, L1)));
    ui->tab5leSigmaMax1->setText(QString::number(l.SigmaMax(L1)));
    ui->tab5leSigmaMax2->setText(QString::number(l.SigmaMax(L2)));

    std::string warningDiletation = l.WarningDiletation();
    std::string warningPreheating = l.WarningPreheating();

    if(warningDiletation.length() || warningPreheating.length()) {
        error.displayWarningMessage(warningDiletation + warningPreheating);
    }
}

void MainWindow::on_tab6btnCalculate_clicked()
{
    auto error = tab6CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    double L1 = ui->tab6leDistToL1->text().toDouble();
    double L2 = ui->tab6leDistToL2->text().toDouble();
    double angle = 90;
    Zcompesation z(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                   DesignWaterPressure, type, Series, DN, CoverDepth, L1, L2, angle);

    ui->tab6leFoamPads1Ldz1->setText(QString::number(z.FoamPads1dz(L1, L2)));
    ui->tab6leFoamPads2Ldz1->setText(QString::number(z.FoamPads2dz(L1, L2)));
    ui->tab6leFoamPads3Ldz1->setText(QString::number(z.FoamPads3dz(L1, L2)));

    ui->tab6leFoamPads1Ldz2->setText(QString::number(z.FoamPads1dz(L2, L1)));
    ui->tab6leFoamPads2Ldz2->setText(QString::number(z.FoamPads2dz(L2, L1)));
    ui->tab6leFoamPads3Ldz2->setText(QString::number(z.FoamPads3dz(L2, L1)));

    ui->tab6leFoamPads1Lz1->setText(QString::number(z.FoamPadsZ(L1, L2)[0]));
    ui->tab6leFoamPads2Lz1->setText(QString::number(z.FoamPadsZ(L1, L2)[1]));
    ui->tab6leFoamPads3Lz1->setText(QString::number(z.FoamPadsZ(L1, L2)[2]));

    ui->tab6leFoamPads1Lz2->setText(QString::number(z.FoamPadsZ(L2, L1)[0]));
    ui->tab6leFoamPads2Lz2->setText(QString::number(z.FoamPadsZ(L2, L1)[1]));
    ui->tab6leFoamPads3Lz2->setText(QString::number(z.FoamPadsZ(L2, L1)[2]));

    ui->tab6leLz1Length->setText(QString::number(z.LzLength(L1, L2)));
    ui->tab6leLz2Length->setText(QString::number(z.LzLength(L2, L1)));

    std::string warningDiletation = z.WarningDiletation();
    std::string warningLength = z.WarningZ();
    std::string warningPreheating = z.WarningPreheating();

    if(warningDiletation.length() || warningLength.length() || warningPreheating.length()) {
        error.displayWarningMessage(warningDiletation + warningLength + warningPreheating);
    }
}

void MainWindow::on_tab7btnCalculate_clicked()
{
    auto error = tab7CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    double L1 = ui->tab7leDistToL1->text().toDouble();
    double L2 = ui->tab7leDistToL2->text().toDouble();
    double angle = 90;
    Ucompesation u(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                   DesignWaterPressure, type, Series, DN, CoverDepth, L1, L2, angle);

    ui->tab7leFoamPads1Ldz1->setText(QString::number(u.FoamPads1dz(L1, L2)));
    ui->tab7leFoamPads2Ldz1->setText(QString::number(u.FoamPads2dz(L1, L2)));
    ui->tab7leFoamPads3Ldz1->setText(QString::number(u.FoamPads3dz(L1, L2)));

    ui->tab7leFoamPads1Ldz2->setText(QString::number(u.FoamPads1dz(L2, L1)));
    ui->tab7leFoamPads2Ldz2->setText(QString::number(u.FoamPads2dz(L2, L1)));
    ui->tab7leFoamPads3Ldz2->setText(QString::number(u.FoamPads3dz(L2, L1)));

    ui->tab7leFoamPads1Lu1->setText(QString::number(u.FoamPads1u1(L1, L2)));
    ui->tab7leFoamPads2Lz1->setText(QString::number(u.FoamPads2u1(L1, L2)));
    ui->tab7leFoamPads3Lu1->setText(QString::number(u.FoamPads3u1(L1, L2)));

    ui->tab7leFoamPads1Lu2->setText(QString::number(u.FoamPads1u1(L2, L1)));
    ui->tab7leFoamPads2Lu2->setText(QString::number(u.FoamPads2u1(L2, L1)));
    ui->tab7leFoamPads3Lu2->setText(QString::number(u.FoamPads3u1(L2, L1)));

    ui->tab7leLateralLength->setText(QString::number(u.LateralLength()));
    ui->tab7leVertexLength->setText(QString::number(u.VertexLength()));

    std::string warningDiletation = u.WarningDiletation();
    std::string warningLength = u.WarningU();
    std::string warningPreheating = u.WarningPreheating();

    if(warningDiletation.length() || warningLength.length() || warningPreheating.length()) {
        error.displayWarningMessage(warningDiletation + warningLength + warningPreheating);
    }
}


void MainWindow::on_tab8btnCalculate_clicked()
{
    auto error = tab8CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    double mainCoverDepth = ui->tab8leCoverDepthMain->text().toDouble();
    int mainDN = ui->tab8cbNominalSizeMain->currentText().remove(0, 3).toInt();
    int mainSeries = ui->tab8cbInstallationClassMain->currentText().remove(0, 7).toInt();
    int mainType = ui->tab8cbPipeTypeMain->currentText() == "Single pipe" ? 1 : 2;
    double mainL = ui->tab8leLMain->text().toDouble();

    double branchCoverDepth = ui->tab8leCoverDepthBranch->text().toDouble();
    int branchDN = ui->tab8cbNominalSizeBranch->currentText().remove(0, 3).toInt();
    int branchSeries = ui->tab8cbInstallationClassBranch->currentText().remove(0, 7).toInt();
    int branchType = ui->tab8cbPipeTypeBranch->currentText() == "Single pipe" ? 1 : 2;
    double branchLa = ui->tab8leLa->text().toDouble();
    double branchLt = ui->tab8leLt->text().toDouble();



    Tcompesation t(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                   DesignWaterPressure, mainType, mainSeries, mainDN, mainCoverDepth, mainL, branchLa, branchLt,
                   branchType, branchSeries, branchDN, branchCoverDepth);

    ui->tab8leFoamPads1->setText(QString::number(t.FoamPads1()));
    ui->tab8leFoamPads2->setText(QString::number(t.FoamPads2()));
    ui->tab8leFoamPads3->setText(QString::number(t.FoamPads3()));
    ui->tab8leSigmaMax->setText(QString::number(t.SigmaMax()));
    ui->tab8leDeltaL->setText(QString::number(t.ExpansionTBranch()));

    std::string warningDiletation = t.WarningDiletation();
    std::string warningLength = t.WarningLength();
    std::string warningPreheating = t.WarningPreheating();

    if(warningDiletation.length() || warningLength.length() || warningPreheating.length()) {
        error.displayWarningMessage(warningDiletation + warningLength + warningPreheating);
    }
}

void MainWindow::on_tab8btnCalculate_2_clicked()
{
    auto error = tab9CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    double mainCoverDepth = ui->tab9leCoverDepthMain->text().toDouble();
    int mainDN = ui->tab9cbNominalSizeMain->currentText().remove(0, 3).toInt();
    int mainSeries = ui->tab9cbInstallationClassMain->currentText().remove(0, 7).toInt();
    int mainType = 1;
    double mainL = ui->tab9leLMain->text().toDouble();

    double branchCoverDepth = ui->tab9leCoverDepthBranch->text().toDouble();
    int branchDN = ui->tab9cbNominalSizeBranch->currentText().remove(0, 3).toInt();
    int branchSeries = ui->tab9cbInstallationClassBranch->currentText().remove(0, 7).toInt();
    int branchType = 1;
    double branchLa = ui->tab9leLa->text().toDouble();
    double branchLt = ui->tab9leLt->text().toDouble();

    Pcompesation p(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                   DesignWaterPressure, mainType, mainSeries, mainDN, mainCoverDepth, mainL, branchLa, branchLt,
                   branchType, branchSeries, branchDN, branchCoverDepth);

    ui->tab9leFoamPads1DLT->setText(QString::number(p.FoamPads1p()));
    ui->tab9leFoamPads2DLT->setText(QString::number(p.FoamPads2a()));
    ui->tab9leFoamPads3DLT->setText(QString::number(p.FoamPads3a()));

    ui->tab9leFoamPads1DLa->setText(QString::number(p.FoamPads1a()));
    ui->tab9leFoamPads2DLa->setText(QString::number(p.FoamPads2a()));
    ui->tab9leFoamPads3DLa->setText(QString::number(p.FoamPads3a()));

    ui->tab9leSigmaMax->setText(QString::number(p.SigmaMax()));
    ui->tab9leDeltaLa->setText(QString::number(p.ExpansionABranch()));
    ui->tab9leDeltaLt->setText(QString::number(p.ExpansionTBranch()));

    std::string warningDiletation = p.WarningDiletationP();
    std::string warningPreheating = p.WarningPreheating();

    if(warningDiletation.length()  || warningPreheating.length()) {
        error.displayWarningMessage(warningDiletation + warningPreheating);
    }
}

void MainWindow::on_tab10btnCalculate_clicked()
{
    auto error = tab10CheckInput();
    if(error.hasInputError()) {
        error.displayEmptyInputMessage();
        return;
    }

    if(error.hasTypeError()) {
        error.displayInvalidInputMessage();
        return;
    }

    MainWindow::getSharedData();
    double L = ui->tab10leDistToL1->text().toDouble();

    Ecompesation e(FlowTemperature, ReturnTemperature, InstallTemperature, SoilDensity, InternalFrictionAngleOfSoil,
                   DesignWaterPressure, type, Series, DN, CoverDepth, L);

    ui->tab10leLe->setText(QString::number(e.LE()));
    ui->tab10leDeltaLe->setText(QString::number(e.deltaLE()));
    ui->tab10leLe_p->setText(QString::number(e.LprimE()));
    ui->tab10leDeltaLe_p->setText(QString::number(e.deltaLprimE()));
    ui->tab10leLb->setText(QString::number(e.LB()));
    ui->tab10leDeltaLb->setText(QString::number(e.deltaLB()));
    ui->tab10leDeltaLb_p->setText(QString::number(e.deltaLprimB()));
    ui->tab10leNo->setText(QString::number(e.No()));
}

