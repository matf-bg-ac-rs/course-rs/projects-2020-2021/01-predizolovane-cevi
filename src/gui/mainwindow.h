#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include "headers/ErrorMessage.hpp"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_tab1btnApply_clicked();

    void on_tab4btnCalculate_clicked();
    void on_tab5btnCalculate_clicked();
    void on_tab6btnCalculate_clicked();
    void on_tab7btnCalculate_clicked();

    void on_tab3btnCalculate_clicked();

    void on_tab8btnCalculate_clicked();

    void on_tab8btnCalculate_2_clicked();

    void on_tab10btnCalculate_clicked();

    void on_tab2btnCalculate_clicked();

private:
    Ui::MainWindow *ui;

    void disableTabs();
    void enableTabs();
    void getSharedData();


    QString Project;
    int CalculationRefNumber;
    QString CalculationRefNumberMark;
    QString CalculationsCarriedOutBy;
    QString Date;

    double FlowTemperature;
    double ReturnTemperature;
    double InstallTemperature;
    double SoilDensity;
    double InternalFrictionAngleOfSoil;
    double DesignWaterPressure;

    int DN;
    int Series;
    int type;
    double CoverDepth;


    ErrorMessage tab1CheckInput();
    ErrorMessage tab2CheckInput();
    ErrorMessage tab3CheckInput();
    ErrorMessage tab4CheckInput();
    ErrorMessage tab5CheckInput();
    ErrorMessage tab6CheckInput();
    ErrorMessage tab7CheckInput();
    ErrorMessage tab8CheckInput();
    ErrorMessage tab9CheckInput();
    ErrorMessage tab10CheckInput();

};
#endif // MAINWINDOW_H
