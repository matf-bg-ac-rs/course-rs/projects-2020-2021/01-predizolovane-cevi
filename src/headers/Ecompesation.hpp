#ifndef ECOMPESATION
#define ECOMPESATION

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"
#include "FrictionalLength.hpp"

class Ecompesation : public FrictionalLength
{
public:

	Ecompesation(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, double L,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: FrictionalLength(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	type, Series, DN, CoverDepth, Name, CalcRefNum, CarriedOutBy, Date), m_L(L)
		{
        }
    // F47
    double PreheatTemp()
    {
    	return m_InstallTemp+m_Stress/(m_ThermalElongationCoeff*m_YoungsFlexibilityModulus);
    }
    // AK
    double LE()
    {
    	double fforce = FrictionForce();
		double sa;
		
		if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
		
    	return 2*(2*m_Stress-m_ThermalElongationCoeff*m_YoungsFlexibilityModulus*(m_FlowTemp-m_InstallTemp))*sa/fforce;
    }
    // AL
    double LB()
    {
    	return 0.5*LE()+FrictionLength();
    }
    // AM
    double deltaLE()
    {
    	double fforce = FrictionForce();
		double sa;
		
		if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
		
    	double f1 = m_ThermalElongationCoeff*(PreheatTemp()-m_InstallTemp)*0.5*LE()*1000;
    	double f2 = fforce*0.5*0.5*LE()*LE()*1000/(2*m_YoungsFlexibilityModulus*sa);
    	return 2 * (f1 - f2);
    }
    // AN
    double deltaLB()
    {
    	double fforce = FrictionForce();
		double sa;
		
		if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
		
    	double f1 = m_ThermalElongationCoeff*(PreheatTemp()-m_InstallTemp)*0.5*LB()*1000;
    	double f2 = fforce*0.5*0.05*LB()*LB()*1000/(2*m_YoungsFlexibilityModulus*sa)-0.5*deltaLE();
    	return f1 - f2;
    }
    // AO
    double No()
    {
    	return (m_L-(2*FrictionLength()))/LE();
    }
	// AP
	double LprimE()
	{
		return ceil((m_L-2*LB())/(No()-1));
	}
	// AQ
	double deltaLprimE()
	{
		double fforce = FrictionForce();
		double sa;
		
		if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
		
    	double f1 = m_ThermalElongationCoeff*(PreheatTemp()-m_InstallTemp)*0.5*LprimE()*1000;
    	double f2 = fforce*0.5*0.5*LprimE()*LprimE()*1000/(2*m_YoungsFlexibilityModulus*sa);
    	return 2 * (f1 - f2);
	}
	// AR
    double deltaLprimB()
    {
    	double fforce = FrictionForce();
		double sa;
		
		if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
		
    	double f1 = m_ThermalElongationCoeff*(PreheatTemp()-m_InstallTemp)*0.5*LB()*1000;
    	double f2 = fforce*0.5*0.05*LB()*LB()*1000/(2*m_YoungsFlexibilityModulus*sa)-0.5*deltaLprimE();
    	return f1 - f2;
    }
	
    //geteri
	double L() const { return m_L; }
	//seteri
	void setL(double L) { m_L = L; }
	
private:
	
	double m_L;
};

#endif
