#ifndef PCOMPESATION
#define PCOMPESATION

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"
#include "FrictionalLength.hpp"
#include "Tcompesation.hpp"

class Pcompesation : public Tcompesation
{
public:
	Pcompesation(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, 
                 double L, double La, double Lt, int atype, int aSeries, int aDN, double aCoverDepth,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: Tcompesation(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	type, Series, DN, CoverDepth, L, La, Lt, atype, aSeries, aDN, aCoverDepth, Name, CalcRefNum, CarriedOutBy, Date)
		{
        }
	//kolona GE
	double UnrestrictedExpansionLa()
    {
    	double temp = m_SingleMeanTemp;
		if (Preheating())
			temp = m_PrestressingTemp - m_InstallTemp;
    	return (m_ThermalElongationCoeff*(m_La/2)*temp)*1000;
    }
    //kolona GC
    double ExpansionABranch()
    {	
    	double ue = UnrestrictedExpansionLa();
    	FrictionalLength f = FrictionalLength(m_FlowTemp, m_ReturnTemp, m_InstallTemp, m_SoilDensity, m_InternalSoilFrictionAngle,
        									  m_DesignWaterPressure, m_atype, m_aSeries, m_aDN, m_aCoverDepth);
    	double fforce = f.FrictionForce();
    	double sa;
    	
    	if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
    	
    	return (ue/1000.0-(fforce*m_La*m_La/4)/(2*sa*m_YoungsFlexibilityModulus))*1000;
    }
    //kolona GD
    double DilatationZonePa()
    {
    	double eab = ExpansionABranch();
    	double carrier;
		
		if (m_atype == 1)
			carrier = m_SinglePipes[m_aDN].Carrier();
		else
			carrier = m_TwinPipes[m_aDN].Carrier();
    	
    	return round(100.0*(1.2*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000*sqrt(eab*carrier)))/100.0;
    }
	//kolona GL
	double DilatationZoneLa()
	{
		double dzp = DilatationZonePa();
		double p1 = m_SinglePipes[m_aDN].p1();
		double p2 = m_SinglePipes[m_aDN].p2();
		
		if (dzp>p2)
			return ceil(p1);
		return p2;
	}
	//kolona GQ
	double DeltaLr()
	{	
		double etb = ExpansionTBranch();
		double eab = ExpansionABranch();
		return sqrt(etb*etb+eab*eab);
	}
	//kolona GO
    double FoamPads3a()
	{
		double dlr = DeltaLr();
		double carrier;
		
		if (m_atype == 1)
			carrier = m_SinglePipes[m_aDN].Carrier();
		else
			carrier = m_TwinPipes[m_aDN].Carrier();
		
		if (dlr>46)
			return ceil(1/1000*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(dlr*carrier)-sqrt(46*carrier)));
		return 0;
	}
	//kolona GN
	double FoamPads2a()
	{
		double fp3 = FoamPads3a();
		double dlr = DeltaLr();
		double carrier;
		
		if (m_atype == 1)
			carrier = m_SinglePipes[m_aDN].Carrier();
		else
			carrier = m_TwinPipes[m_aDN].Carrier();
		
		if (dlr>23)
			return ceil(1/1000*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(dlr*carrier)-sqrt(23*carrier)-fp3));
		return 0;
	}
	//kolona GM
	double FoamPads1a()
	{
		double fp3 = FoamPads3a();
		double fp2 = FoamPads2a();
		double dlr = DeltaLr();
		double dz = DilatationZoneLa();
		
		if (dlr <= 23)
			return ceil(dz);
		if (dz-(fp2+fp3) > 0)
			return ceil(dz-(fp2+fp3));
		return 0;
	}
	//kolona GH
	double FoamPads1p()
	{
		double fp3 = FoamPads3a();
		double fp2 = FoamPads2a();
		double dlr = DeltaLr();
		double dz = DilatationZoneLt();
		
		if (dlr <= 23)
			return ceil(dz);
		if (dz-(fp2+fp3) > 0)
			return ceil(dz-(fp2+fp3));
		return 0;
	}
	
    std::string WarningDiletationP()
	{
		double eab = ExpansionABranch();
        std::string msg = "";
	
		if (eab >= 81)
		{
            msg = "Warning Diletation too high\n";
		}
        return msg;
	}
};

#endif
