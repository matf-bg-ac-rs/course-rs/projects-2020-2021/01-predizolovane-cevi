#ifndef PRESSURELOSS_HPP
#define PRESSURELOSS_HPP

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <algorithm>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"

double cp = 4.187;
double epsilon = 0.000035;

class PressureLoss : public Project
{
public:

	PressureLoss(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, double length, double Q,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: Project(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	      Name, CalcRefNum, CarriedOutBy, Date), m_type(type), m_Series(Series), m_DN(DN), m_CoverDepth(CoverDepth),
        	      m_length(length), m_Q(Q)
		{
			m_t.push_back(10);
			for(int i=0; i<43; i++)
				m_t.push_back(m_t[i]+2.5);
			
			m_rho.push_back(999.7);
			std::vector<double> delta_rho = {0.375, 0.625, 0.875, 1.025, 1.25, 1.325, 1.5, 1.625, 1.725, 1.725, 1.725};
			for(int i=0; i<43; i++)
				m_rho.push_back(m_rho[i]-delta_rho[i/4]);
			
			m_ni.push_back(0.000001306);
			std::vector<double> delta_ni = {0.000000075, 0.00000005025, 0.0000000365, 0.00000002575, 0.0000000195, 0.00000001575,
											0.0000000125, 0.00000000975, 0.00000000775, 0.00000000775, 0.00000000775,};
			for(int i=0; i<43; i++)
				m_ni.push_back(m_ni[i]-delta_ni[i/4]);
        }
    
    double T_s(){
        return (FlowTemp() + ReturnTemp())/2.0;
    }
    
    // BI
    double du()
    {
    	double carrier, s;
    	if (m_type == 1)
		{
			carrier = m_SinglePipes[m_DN].Carrier();
			s = m_SinglePipes[m_DN].s();
		}
		else
		{
			carrier = m_TwinPipes[m_DN].Carrier();
			s = m_TwinPipes[m_DN].s();
		}
		return (carrier-2*s)/1000.0;
    }
    // BJ
    double w()
    {	
    	auto it = std::find(m_t.begin(), m_t.end(), T_s());
    	double rho_Ts = m_rho[int(it - m_t.begin())];
    	return m_Q/1000.0/(cp*du()*du()*PI/4*rho_Ts*m_TwinMeanTemp);
    }
    // BK
    double Re()
    {
    	auto it = std::find(m_t.begin(), m_t.end(), T_s());
    	double ni_Ts = m_ni[int(it - m_t.begin())];
    	return du()*w()/ni_Ts;
    }
    // BL
    double R1()
    {
    	return 568*du()/epsilon;
    }
    // BM
    double lambda()
    {
    	if(Re() <= 2320)
    		return 64.0/Re();
    	if(Re() >= R1())
    		return pow(epsilon/du(), 0.25)*0.115;
    	return pow(epsilon/du() + 60.0/Re(), 0.25)*0.115;
    }
    // BN
    double R()
    {
    	auto it = std::find(m_t.begin(), m_t.end(), T_s());
    	double rho_Ts = m_rho[int(it - m_t.begin())];
    	return lambda()*rho_Ts*pow(w(),2)/du()/2;
    }
    // BG
    double V()
    {
    	auto it = std::find(m_t.begin(), m_t.end(), T_s());
    	double rho_Ts = m_rho[int(it - m_t.begin())];
    	return m_Q/rho_Ts/cp/m_TwinMeanTemp;
    }
    // BO
    double RxL()
    {
    	return R()*m_length;
    }
    // ogranicenje za brzinu
    std::string WarningW(double constraint)
    {
        if (w() >= constraint)
        {
            return "Warning: dw too high\n";
        }
        return "";
    }
    // ogranicenje za pritisak
    std::string WarningR(double constraint)
    {
        if (R() >= constraint)
        {
            return "Warning: R too high\n";
        }
        return "";
    }

    //geteri
	int type() const { return m_type; }
	int Series() const { return m_Series; }
	int DN() const { return m_DN; }
	double CoverDepth() const { return m_CoverDepth; }
	double length() const { return m_length; }
	double Q() const { return m_Q; }
	std::vector<double> t() const { return m_t; }
    std::vector<double> rho() const { return m_rho; }
    std::vector<double> ni() const { return m_ni; }
    
	//seteri
	void setType(int type) { m_type = type; }
	void setSeries(int Series) { m_Series = Series; }
	void setDN(int DN) { m_DN = DN; }
	void setCoverDepth(double CoverDepth) { m_CoverDepth = CoverDepth; }
	void setlength(double length) { m_length = length; }
	void setQ(double Q) { m_Q = Q; }
	
protected:
	
	// Promenljive
	// type 1 - Single, 2 - Twin
	int m_type;
	int m_Series;
	int m_DN;
	double m_CoverDepth;
	double m_length;
	double m_Q;
	std::vector<double> m_t;
	std::vector<double> m_rho;
	std::vector<double> m_ni;
};

#endif
