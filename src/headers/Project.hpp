#ifndef PROJECT_HPP
#define PROJECT_HPP

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"

#define PI 3.141592653589793238463
#define GravityCoeff 9.81
#define DensityPUR  60.0
#define DensitySteel 7850.0
#define DensityPEHD 960.0
#define DensityWater 1000.0

class Project {

        public:

                ~Project() = default;

                Project(
                        double FlowTemp,
                        double ReturnTemp,
                        double InstallTemp,
                        double SoilDensity,
                        double InternalSoilFrictionAngle,
                        double DesignWaterPressure,
                        std::string Name = "",
                        std::string CalcRefNum = "",
                        std::string CarriedOutBy = "",
                        std::string Date = ""
                ) : 
                        m_FlowTemp(FlowTemp),
                        m_ReturnTemp(ReturnTemp),
                        m_InstallTemp(InstallTemp),
                        m_SoilDensity(SoilDensity),
                        m_InternalSoilFrictionAngle(InternalSoilFrictionAngle),
                        m_DesignWaterPressure(DesignWaterPressure),
                        m_Name(Name),
                        m_CalcRefNum(CalcRefNum),
                        m_CarriedOutBy(CarriedOutBy),
                        m_Date(Date)
                {

                        // Racunanje promenljivih zavisnih od project info
                        recalculate();
                        // Unos cevi
                        std::ifstream single;
                    single.open("../src/data/singlePipes.txt");
                    for (int i=0; i<18; i++)
	                {
	        	        int DN, Series1, Series2, Series3, A, e, p1, p2;
                        double Carrier, s, s1, s2, s3, I, II, III;
		
                        single >> DN >> Carrier >> s >> Series1 >> Series2 >> Series3 >> s1 >> s2 >> s3 >> A >> e >> p1 >> p2 >> I >> II >> III;
                        SinglePipe sp(DN, Carrier, s, Series1, Series2, Series3, s1, s2, s3, A, e, p1, p2, DesignWaterPressure, I, II, III);
	        	        m_SinglePipes.insert({DN, sp});
	                }
	
	                std::ifstream twin;
                    twin.open("../src/data/twinPipes.txt");
                    for (int i=0; i<12; i++)
	                {
		                int DN, Series1, Series2, e, Lp;
                        double Carrier, s, s1, s2, I, II;
		
                        twin >> DN >> Carrier >> s >> Series1 >> Series2 >> s1 >> s2 >> e >> Lp >> I >> II;
                        TwinPipe tp(DN, Carrier, s, Series1, Series2, s1, s2, e, Lp, DesignWaterPressure, I, II);
		                m_TwinPipes.insert({DN, tp});
	                }

                        single.close();
                        twin.close();
                }

                void recalculate() {
                        m_RestPressureCoeff = 1 - sin(m_InternalSoilFrictionAngle * PI/180);
                        m_AngleOfInterfaceFriction = m_InternalSoilFrictionAngle * 2.0/3;
                        m_FrictionCoeff = tan(m_AngleOfInterfaceFriction * PI/180);
                        m_Stress = (235 - (m_FlowTemp - 50)/3)/1.12;
                        m_ThermalElongationCoeff = (11.4 + m_FlowTemp/129)/(1000000);
                        m_YoungsFlexibilityModulus = (21.4 - m_FlowTemp/175)*(10000);
                        m_SingleMeanTemp = m_FlowTemp - m_InstallTemp;
                        m_TwinMeanTemp = m_FlowTemp - m_ReturnTemp;
                        m_TwinMeanTempM = (m_FlowTemp + m_ReturnTemp)/2 - m_InstallTemp;
                        m_PrestressingTemp = (m_FlowTemp + m_InstallTemp)/2 + 5;
                }

                std::string Name() const { return m_Name; }
                std::string CalcRefNum() const { return m_CalcRefNum; }
                std::string CarriedOutBy() const { return m_CarriedOutBy; }
                std::string Date() const { return m_Date; }
                double FlowTemp() const { return m_FlowTemp; }
                double ReturnTemp() const { return m_ReturnTemp; }
                double InstallTemp() const { return m_InstallTemp; }
                double SoilDensity() const { return m_SoilDensity; }
                double InternalSoilFrictionAngle() const { return m_InternalSoilFrictionAngle; }
                double DesignWaterPressure() const { return m_DesignWaterPressure; }

                std::unordered_map<int, SinglePipe> SinglePipes() const { return m_SinglePipes; }
                std::unordered_map<int, TwinPipe> TwinPipes() const { return m_TwinPipes; }

                // Getteri za promenljive
                double FrictionCoeff() const { return m_FrictionCoeff; }
                double RestPressureCoef() const { return  m_RestPressureCoeff; }
                double AngleOfInterfaceFriction() const { return m_AngleOfInterfaceFriction; }
                double Stress() const { return m_Stress; }
                double ThermalElongationCoeff() const { return m_ThermalElongationCoeff; }
                double YoungsFlexibilityModulus() const { return m_YoungsFlexibilityModulus; }
                double SingleMeanTemp() const { return m_SingleMeanTemp; }
                double TwinMeanTemp() const { return m_TwinMeanTemp; }
                double TwinMeanTempM() const { return m_TwinMeanTempM; }
                double PrestressingTemp() const { return m_PrestressingTemp; }

                // Setteri za zuta polja - inpute
                // TODO treba uradit proveru vrednosti ako postoje neke granice

                void setFlowTemp(double temp) {
                        m_FlowTemp = temp;
                        recalculate();
                }

                void setReturnTemp(double temp) {
                        m_ReturnTemp = temp;
                        recalculate();
                }

                void setInstallTemp(double temp) {
                        m_InstallTemp = temp;
                        recalculate();
                }

                void setSoilDensity(double density) {
                        m_SoilDensity = density;
                        recalculate();
                }

                void setInternalSoilFrictionAngle(double angle) {
                        m_InternalSoilFrictionAngle = angle;
                        recalculate();
                }

                void setDesignWaterPressure(double pressure) {
                        m_DesignWaterPressure = pressure;
                        recalculate();
                }


        protected:

                // Project info
                double m_FlowTemp;
                double m_ReturnTemp;
                double m_InstallTemp;
                double m_SoilDensity;
                double m_InternalSoilFrictionAngle;
                double m_DesignWaterPressure;
                std::string m_Name;
                std::string m_CalcRefNum;
                std::string m_CarriedOutBy;
                std::string m_Date;

                // Promenljive
                double m_FrictionCoeff;
                double m_RestPressureCoeff;
                double m_AngleOfInterfaceFriction;
                double m_Stress;
                double m_ThermalElongationCoeff;
                double m_YoungsFlexibilityModulus;

                // Temperature
                double m_SingleMeanTemp;
                double m_TwinMeanTemp;
                double m_TwinMeanTempM;
                double m_PrestressingTemp;

                // Cevi
                std::unordered_map<int, SinglePipe> m_SinglePipes;
                std::unordered_map<int, TwinPipe> m_TwinPipes;
};

#endif
