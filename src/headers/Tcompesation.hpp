#ifndef TCOMPESATION
#define TCOMPESATION

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"
#include "FrictionalLength.hpp"

class Tcompesation : public FrictionalLength
{
public:
	Tcompesation(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, 
                 double L, double La, double Lt, int atype, int aSeries, int aDN, double aCoverDepth,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: FrictionalLength(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	type, Series, DN, CoverDepth, Name, CalcRefNum, CarriedOutBy, Date), 
        	m_L(L), m_La(La), m_Lt(Lt), m_atype(atype), m_aSeries(aSeries), m_aDN(aDN), m_aCoverDepth(aCoverDepth)
		{
        }
    // Da li je potrebno predgrevanje
    bool Preheating()
    {
    	double flength = FrictionLength();
    	return m_L > flength;
    }
    //kolona FM
    double UnrestrictedExpansion()
    {
    	double temp = m_SingleMeanTemp;
    	double l = m_L;
		if (Preheating())
		{
			temp = m_PrestressingTemp - m_InstallTemp;
			l = FrictionLength();
		}
		else if (m_type == 2)
			temp = m_TwinMeanTempM;
    	//std::cout << l << " " << temp << "\n";
    	return (m_ThermalElongationCoeff*(l-m_Lt)*temp)*1000;
    }
    //kolona FN
    double ExpansionTBranch()
    {
    	double ue = UnrestrictedExpansion();
    	double fforce = FrictionForce();
    	double sa;
    	double l = m_L;
		if (Preheating())
			l = FrictionLength();
    	
    	if (m_type == 1)
			sa = m_SinglePipes[m_DN].SectionalArea();
		else
			sa = m_TwinPipes[m_DN].SectionalArea();
    	
    	return (ue/1000.0-((fforce*(l+m_Lt)*(l-m_Lt))/(2*sa*m_YoungsFlexibilityModulus)))*1000;
    }
    //kolona FP
    double DilatationZoneLt()
    {
    	double etb = ExpansionTBranch();
    	double carrier;
    	
    	if (m_atype == 1)
			carrier = m_SinglePipes[m_aDN].Carrier();
		else
			carrier = m_TwinPipes[m_aDN].Carrier();
    	
    	return std::round(100.0*1.2*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000*sqrt(etb*carrier))/100.0;
    }
    //kolona FS
    double FoamPads3()
	{
		double etb = ExpansionTBranch();
		double carrier;
		
		if (m_atype == 1)
			carrier = m_SinglePipes[m_aDN].Carrier();
		else
			carrier = m_TwinPipes[m_aDN].Carrier();
		
		if (etb > 54)
			return ceil(1/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(etb*carrier)-sqrt(54*carrier)));
		return 0;
	}
	//kolona FR
	double FoamPads2()
	{
		double fp3 = FoamPads3();
		double etb = ExpansionTBranch();
		double carrier;
		
		if (m_atype == 1)
			carrier = m_SinglePipes[m_aDN].Carrier();
		else
			carrier = m_TwinPipes[m_aDN].Carrier();
		
		if (etb > 27)
			return ceil(1/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(etb*carrier)-sqrt(27*carrier)-fp3));
		return 0;
	}
	//kolona FQ
	double FoamPads1()
	{
		double fp3 = FoamPads3();
		double fp2 = FoamPads2();
		double etb = ExpansionTBranch();
		double dz = DilatationZoneLt();
		
		if (etb <= 27)
			return ceil(dz);
		if (dz-(fp2+fp3) > 0)
			return ceil(dz-(fp2+fp3));
		return 0;
	}
	
	//geteri
	double L() const { return m_L; }
	double La() const { return m_La; }
	double Lt() const { return m_Lt; }
	int atype() const { return m_atype; }
	int aSeries() const { return m_aSeries; }
	int aDN() const { return m_aDN; }
	double aCoverDepth() const { return m_aCoverDepth; }
	
	//seteri
	void setL(double L) { m_L = L; }
	void setLa(double La) { m_La = La; }
	void setLt(double Lt) { m_Lt = Lt; }
	void setAType(int type) { m_atype = type; }
	void setASeries(int Series) { m_aSeries = Series; }
	void setADN(int DN) { m_aDN = DN; }
	void setACoverDepth(double CoverDepth) { m_aCoverDepth = CoverDepth; }
	
	//ovo nema u tabelama
	double SigmaMax()
	{
		double L = DilatationZoneLt();
		FrictionalLength f = FrictionalLength(m_FlowTemp, m_ReturnTemp, m_InstallTemp, m_SoilDensity, m_InternalSoilFrictionAngle,
        									  m_DesignWaterPressure, m_atype, m_aSeries, m_aDN, m_aCoverDepth);
    	double fforce = f.FrictionForce();
		double sa;
		
		if (m_type == 1)
			sa = m_SinglePipes[m_aDN].SectionalArea();
		else
			sa = m_TwinPipes[m_aDN].SectionalArea();
		
		return (L*fforce)/sa;
	}
	
    //upozorenje za prevelike diletacije
    std::string WarningDiletation()
	{
		double etb = ExpansionTBranch();
        std::string msg = "";
		if (etb >= 54)
		{
            msg = "Warning Diletation too high.\n";
		}
        return msg;
	}
	//upozorenje za duzine
    std::string WarningLength()
	{
		double e = m_SinglePipes[m_aDN].e();
        std::string msg = "";
		if (e < m_La)
		{
            msg = "Warning La too long.\n";
		}
        return msg;
	}
    std::string WarningPreheating()
    {
        std::string msg = "";
        if (Preheating())
        {
            msg = "Warning preheating is needed.\n";
        }
        return msg;
    }
    
protected:
	
	double m_L;
	double m_La;
	double m_Lt;
	int m_atype;
	int m_aSeries;
	int m_aDN;
	double m_aCoverDepth;
};

#endif
