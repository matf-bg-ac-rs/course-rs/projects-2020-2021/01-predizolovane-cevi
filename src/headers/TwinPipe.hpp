#ifndef TWINPIPE_HPP
#define TWINPIPE_HPP

#include "Pipe.hpp"
#include <cmath>
#define PI 3.141592653589793238463

class TwinPipe : public Pipe
{
public:
    TwinPipe(int DN, double Carrier, double s, int Series1, int Series2, double s1, double s2, int e, int Lp, double DesignWaterPressure,
            double I = 0, double II = 0)
        : Pipe(DN, Carrier, s, Series1, Series2, s1, s2, DesignWaterPressure), m_e(e), m_Lp(Lp), m_I(I), m_II(II)
    {
    }
    TwinPipe() = default;
    ~TwinPipe() = default;
    
    double SectionalArea() const override
    {
    	return (m_Carrier - m_s) * PI * m_s * 2;
    }
    
    //DesignWaterPressure se uzima iz project information sheeta polje E20
    double sigma_up() const override
    {
    	return (m_Carrier / m_s - 1) * m_DesignWaterPressure / 4;
    }
    
    double sigma_lp() const override
    {
    	return m_DesignWaterPressure * pow(m_Carrier - 2*m_s, 2) * 0.785 / SectionalArea();
    }
    
    double t() const override
    {
    	return sigma_lp() - (sigma_up() * 0.3);
    }
    
    double water_per_meter() const override
    {
    	return 2 * pow((m_Carrier - 2*m_s)/2, 2) * PI / 1000;
    }
    
    int e() const { return m_e; }
    int Lp() const { return m_Lp; }
    double I() const { return m_I; }
    double II() const { return m_II; }
    
private:
	//etazno ogranicenja 											   --- kolona M
	int m_e;
	//rastojanje izmedju 2 celicne cevi ztraty i tepelne ztraty tabele --- kolona N
	int m_Lp;
    double m_I;
    double m_II;
};

#endif
