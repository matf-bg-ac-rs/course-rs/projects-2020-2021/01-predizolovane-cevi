#ifndef UCOMPESATION
#define UCOMPESATION

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>
#include <vector>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"
#include "FrictionalLength.hpp"
#include "Lcompesation.hpp"
#include "Zcompesation.hpp"

class Ucompesation : public Zcompesation
{
public:

	Ucompesation(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, double L1, double L2, double angle,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: Zcompesation(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	type, Series, DN, CoverDepth, L1, L2, angle, Name, CalcRefNum, CarriedOutBy, Date)
		{
        }
	//kolona DT
	double LateralLengthUtil()
	{
		double de1 = DeltaExpansion(m_L1, m_L2);
		double de2 = DeltaExpansion(m_L2, m_L1);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (de1 < de2)
			return std::round(10*0.8*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000.0*sqrt(de2*carrier))/10.0;
		return std::round(10*0.8*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000.0*sqrt(de1*carrier))/10.0;
	}
	//kolona DU
	double LateralLength(){
		double l = LateralLengthUtil();
		if (l < 2)
			return 2;
		return ceil(l);
	}
	//kolona DU
	double VertexLength(){
        double ll = LateralLength() / 1.5;
		if (ll < 2)
			return 2;
		return ceil(ll);
	}
	//kolona DX
	double FoamPads3u1(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (de > 54)
			return ceil(0.8/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(de*carrier)-sqrt(54*carrier)));
		return 0;
	}
	//kolona DY
	double FoamPads2u1(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double fp3 = FoamPads3u1(L1, L2);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (de > 27)
			return ceil(0.8/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(de*carrier)-sqrt(27*carrier)-fp3));
		return 0;
	}
	//kolona DZ
	double FoamPads1u1(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double ll = LateralLength();
		double fp3 = FoamPads3u1(L1, L2);
		double fp2 = FoamPads2u1(L1, L2);
		
		if (de <= 27)
			return ceil(ll);
		if (ll-(fp2+fp3) > 0)
			return ceil(ll-(fp2+fp3));
		return 0;
	}
	// kolone ED-EL
	std::vector<double> FoamPadsU(double L1, double L2)
	{
		double ll = LateralLength() - 1;
		double fp3 = FoamPads3dz(L1, L2);
		double fp2 = FoamPads2dz(L1, L2);
		double fp1 = FoamPads1dz(L1, L2);
		
		if (fp3 != 0)
			return {0, 0, ll};
		if (fp2 != 0)
			return {0, ll, 0};
		if (fp1 != 0)
			return {ll, 0, 0};
		return {0, 0, 0};
	}
	//upozorenje za duzine krakova
    std::string WarningU()
	{
        std::string msg = "";
		if (m_L1/m_L2 >= 2)
		{
            msg = "Warning L1 is 2 times or more longer than L2\n";
            return msg;
		}
		if (m_L2/m_L1 >= 2)
		{
            msg = "Warning L2 is 2 times or more longer than L1\n";
            return msg;
		}
        return msg;
	}
};

#endif
