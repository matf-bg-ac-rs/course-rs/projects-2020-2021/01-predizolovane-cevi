#ifndef ZCOMPESATION
#define ZCOMPESATION

#include <string>
#include <cmath>
#include <fstream>
#include <unordered_map>
#include <vector>

#include "Pipe.hpp"
#include "SinglePipe.hpp"
#include "TwinPipe.hpp"
#include "Project.hpp"
#include "FrictionalLength.hpp"
#include "Lcompesation.hpp"

class Zcompesation : public Lcompesation
{
public:
	
	Zcompesation(double FlowTemp, double ReturnTemp, double InstallTemp, double SoilDensity, double InternalSoilFrictionAngle,
                 double DesignWaterPressure, int type, int Series, int DN, double CoverDepth, double L1, double L2, double angle,
                 std::string Name = "", std::string CalcRefNum = "", std::string CarriedOutBy = "", std::string Date = "")
		: Lcompesation(FlowTemp, ReturnTemp, InstallTemp, SoilDensity, InternalSoilFrictionAngle, DesignWaterPressure,
        	type, Series, DN, CoverDepth, L1, L2, angle, Name, CalcRefNum, CarriedOutBy, Date)
		{
        }
	//kolona CW
	double LzLength(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		return std::round(10*0.65*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000*sqrt((de)*carrier))/10.0;
	}
	//kolona DF
	double DilatationZoneZ(double L1, double L2)
	{
		double l = LzLength(L1, L2)/2;
		double flength = FrictionLength();
		
		if (flength < l)
			l = flength;
		
		double e = Expansion(l);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		return sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)/1000*sqrt(e*carrier);
	}
	//kolona DI
	double FoamPads3dz(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (de > 54)
			return ceil(0.1/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(de*carrier)-sqrt(54*carrier)));
		return 0;
	}
	//kolona DH
	double FoamPads2dz(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double fp3 = FoamPads3dz(L1, L2);
		double carrier;
		
		if (m_type == 1)
			carrier = m_SinglePipes[m_DN].Carrier();
		else
			carrier = m_TwinPipes[m_DN].Carrier();
		
		if (de > 27)
			return ceil(0.1/1000.0*sqrt(1.5*m_YoungsFlexibilityModulus/m_Stress)*(sqrt(de*carrier)-sqrt(27*carrier)-fp3));
		return 0;
	}
	//kolona DG
	double FoamPads1dz(double L1, double L2)
	{
		double de = DeltaExpansion(L1, L2);
		double dz = DilatationZone(L1, L2) * 0.5;
		double fp3 = FoamPads3dz(L1, L2);
		double fp2 = FoamPads2dz(L1, L2);
		
		if (de <= 27)
			return ceil(dz);
		if (dz-(fp2+fp3) > 0)
			return ceil(dz-(fp2+fp3));
		return 0;
	}
	// kolone CZ-DE
	std::vector<double> FoamPadsZ(double L1, double L2)
	{
		double lzl = LzLength(L1, L2);
		double fp3 = FoamPads3dz(L1, L2);
		double fp2 = FoamPads2dz(L1, L2);
		double fp1 = FoamPads1dz(L1, L2);
		
		if (fp3 != 0)
			return {0, 0, lzl};
		if (fp2 != 0)
			return {0, lzl, 0};
		if (fp1 != 0)
			return {lzl, 0, 0};
		return {0, 0, 0};
	}
	//upozorenje za duzine krakova
    std::string WarningZ()
	{
        std::string msg = "";
		if (m_L1/m_L2 >= 4)
		{
            msg = "Warning L1 is 4 times or more longer than L2\n";
            return msg;
		}
		if (m_L2/m_L1 >= 4)
		{
            msg = "Warning L2 is 4 times or more longer than L1\n";
            return msg;
		}
        return msg;
	}
};

#endif
