QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    gui/mainwindow.cpp \
    main.cpp \


HEADERS += \
    gui/mainwindow.h \
    headers/Ecompesation.hpp \
    headers/EnergyLoss.hpp \
    headers/ErrorMessage.hpp \
    headers/FrictionalLength.hpp \
    headers/Lcompesation.hpp \
    headers/Pcompesation.hpp \
    headers/Pipe.hpp \
    headers/PressureLoss.hpp \
    headers/Project.hpp \
    headers/SinglePipe.hpp \
    headers/Tcompesation.hpp \
    headers/TwinPipe.hpp \
    headers/Ucompesation.hpp \
    headers/Zcompesation.hpp



FORMS += \
    gui/forms/mainwindow.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

