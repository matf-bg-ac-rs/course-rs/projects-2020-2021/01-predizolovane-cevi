#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../headers/Pipe.hpp"
#include "../headers/SinglePipe.hpp"
#include "../headers/TwinPipe.hpp"
#include "../headers/Project.hpp"
#include "../headers/FrictionalLength.hpp"
#include "../headers/Lcompesation.hpp"
#include "../headers/Zcompesation.hpp"
#include "../headers/Ucompesation.hpp"
#include "../headers/Tcompesation.hpp"
#include "../headers/Pcompesation.hpp"
#include "../headers/Ecompesation.hpp"
#include "../headers/PressureLoss.hpp"
 
int main() {

	double cd = 1.0;
	int type = 1, series = 3;
	int dns1[] = {20, 25, 32, 40, 50, 65, 80, 100, 125, 150, 200, 250, 300, 350, 400, 450, 500, 600};
	int dns2[] = {20, 25, 32, 40, 50, 65, 80, 100, 125, 150, 200, 250};
	double l = 100;
	
	std::vector<double> q = {35640,
73791,
150119,
222168,
414759,
815682,
1247299,
2491537,
4377938,
7247283,
14819619,
26822081,
42360157,
54654798,
79414637,
107068790,
142581175,
231898274

};
int i = 0;
	for (int dn : dns1){

		PressureLoss a(90, 70, 10, 1.9, 32.5, 2.5, type, series, dn, cd, l, q[i]);
		std::cout << a.V() << ", " << a.RxL() << "\n";
		i++;
	}
	
	/*
	for (double i=1; i<=195; i+=1){
		l1 = l2 = i/2.0;
		Ucompesation a(130, 70, 10, 1.9, 32.5, 2.5, type, series, 450, cd, l1, l2, 90);
		std::cout << a.LateralLengthUtil() << ", " << a.LateralLength() <<  ", " << a.FoamPads1u1(l1,l2) << ", " << a.FoamPads2u1(l1,l2) << ", " << a.FoamPads3u1(l1,l2) << ", " << a.FoamPadsU(l1,l2)[0] << ", " << a.FoamPadsU(l1,l2)[1] << ", " << a.FoamPadsU(l1,l2)[2] << "\n";
	}
	*/
	/*
	Zcompesation a(130, 70, 10, 1.9, 32.5, 2.5, type, series, 65, cd, l1, l2, 90);

	std::cout << a.DeltaExpansion(l1, l2) << ", " << a.DilatationZone(l1, l2) << ", " << a.FoamPads1(l1,l2) << ", " << a.FoamPads2() << ", " << a.FoamPads3() << ", " << a.SigmaMax(l1) << "\n";
	std::cout << a.DeltaExpansion(l2, l1) << ", " << a.DilatationZone(l2, l1) << ", " << a.FoamPads1(l2,l1) << ", " << a.FoamPads2() << ", " << a.FoamPads3() << ", " << a.SigmaMax(l2) << "\n";
	*/
	/*
	Ucompesation a(130, 70, 10, 1.9, 32.5, 2.5, type, series, 100, cd, l1, l2, 90);
	std::cout << a.LateralLengthUtil() << ", " << a.LateralLength() <<  ", " << a.FoamPads1u1(l1,l2) << ", " << a.FoamPads2u1(l1,l2) << ", " << a.FoamPads3u1(l1,l2) << ", " << a.FoamPadsU(l1,l2)[0] << ", " << a.FoamPadsU(l1,l2)[1] << ", " << a.FoamPadsU(l1,l2)[2] << "\n";
	*/
/*
	PressureLoss a(130, 70, 10, 1.9, 32.5, 2.5, type, series, 25, cd, l, 0.1111);
	std::vector<double> t = a.t();
	std::vector<double> rho = a.rho();
	std::vector<double> ni = a.ni();
	
	for(int i=0; i<44; i++)
		std::cout << t[i] << ", " << rho[i] <<  ", " << ni[i] << "\n";
*/	
	return 0;
} 










